#!/usr/bin/env perl
#===============================================================================
#
#         FILE: bill_validator_test.pl
#
#        USAGE: ./bill_validator_test.pl  
#
#  DESCRIPTION: Программа для проверки возможности работы с купюроприемником 
#  		CasheCode через последовательный порт 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.2
#      CREATED: 04.03.2020 11:20:57
#     REVISION: 11.03.2020
#===============================================================================

use strict;
use warnings;
use utf8;
use Digest::CRC;
use Device::SerialPort;
use Data::Printer;
use 5.010;

my $Configuration_File_Name='conf';
my $PortObj=Device::SerialPort->new("/dev/ttyUSB0");
$PortObj->baudrate(9600);
$PortObj->parity("none");
$PortObj->databits(8);
$PortObj->stopbits(1);
$PortObj-> write_settings || undef $PortObj;
#$PortObj-> read_interval(10);

my %CMD=( 
	'ack' =>'00',
	'nack'=>'ff',
	'reset' => '30',
	'get_status' => '31',
	'set_security' => '32',
	'poll' => '33',
	'enable_bill_types' => '34',
	'stack' => '35',
	'return' => '36',
	'identification' => '37',
	'hold' => '38',
	'set_barcode_parameters' => '39',
	'extract_barcode_data' => '3A',
	'get_bill_table' => '41',
	'download' => '50',
	'get_crc32_of_the_code' => '51',
	'request_statistics' => '60',
);

my $SYNC = '02';# 02h
my $ADR = '03'; #03h
my $LNG = '06';	#06h
my $DATA = '00';#00h
my $POLYNOM = '8408';
my $CRC;
#p $PortObj;
sub cmd_message_prep {
my $CMD = shift;
my $CRC = ccnet_crc16 ($SYNC.$ADR.$LNG.$CMD);
#my $CRC = Digest::CRC::crc16_hex(pack('H*',$SYNC.$ADR.$LNG.$CMD),,,,,$POLYNOM);
my $mess = $SYNC.$ADR.$LNG.$CMD.$CRC;
p $mess;
my $packed_mess = pack('H*', $mess);
return \$packed_mess;
}

#---------------------------------------------------------------------
my $output_string = cmd_message_prep ($CMD{request_statistics});

my $count_out = $PortObj->write($$output_string);
p $count_out;
	my $str= unpack('H*',$$output_string);
	#	use bytes;
	warn "write failed\n"         unless ($str);
	say "mes bytes = ".length($str)/2;
	warn "write incomplete\n"     if ( $count_out != length($str)/2);
	#no bytes;
	#	p $PortObj;
	$PortObj->read_char_time(0);     # don't wait for each character
	$PortObj->read_const_time(200); # 0,01 second per unfulfilled "read" call
	my ($count, $result) = $PortObj->read(255);
	p $result;
	$result = unpack('H*',$result);
	p $result;
    print "count = $count, result = $result\n";
    ##--------------------------------------
    #my $output_string = pack ('H*', $CMD);
    #$PortObj->save($Configuration_File_Name)
    #     || warn "Can't save $Configuration_File_Name: $!\n";
    #     
    #my ($count_in, $string_in) = $PortObj->read($InBytes);
    #
    #     warn "read unsuccessful\n" unless ($count_in == $InBytes);
#---------------------------------------------------------------------
sub ccnet_crc16{               # принимаем строку в hex-ах
my $InDataStr=shift;
my $POLYNOM = 33800;# = 8408h
my @InData = unpack '(a[2])*',$InDataStr;
my $sizeData=@InData;
my $CRC=0;
	for (my $i=0; $i < $sizeData; $i++)
	{
	$CRC^=hex($InData[$i]);
		for (my $j=0; $j<8; $j++)
		{
			if ($CRC&1){
				$CRC>>=1; 
				$CRC^=$POLYNOM;
			}
			else{
				$CRC>>=1; 
			}
		}	
	}
		my @arr = unpack '(a[2])*', sprintf("%x", $CRC); 
		return $arr[1].$arr[0];
}
