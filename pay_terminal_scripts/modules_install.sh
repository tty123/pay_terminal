#! /bin/sh
#
# modules_install.sh
# Copyright (C) 2020 Tolmachev V.N. <>
#
# Distributed under terms of the MIT license.
#

cpan POSIX
cpan Device::SerialPort
cpan Data::Printer
cpan JSON
cpan AnyEvent::HTTPD
cpan DBI
cpan DBD::SQLite
cpan XML::Simple
cpan Text::Template
