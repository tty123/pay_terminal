#! /bin/sh
#
# start.sh
# Copyright (C) 2020 Tolmachev V.N. <>
#
# Distributed under terms of the MIT license.
#

# Make sure only root can run our script
#----------------------------------------
if [[ $EUID -ne 0 ]]; then
	    echo "This script must be run as root"
	        exit 1
	fi
#----------------------------------------	

systemctl --user start chrome.service
systemctl start payterm.service
systemctl start movefiles.service
