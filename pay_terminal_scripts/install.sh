#! /bin/bash
#
# install.sh
# Copyright (C) 2020 Tolmachev V.N. <>
#
# Distributed under terms of the MIT license.
#

# Make sure only root can run our script
#----------------------------------------
if [[ $EUID -ne 0 ]]; then
	    echo "This script must be run as root"
	        exit 1
	fi
#----------------------------------------	
#===== install all required omponents =====

#-- Web component --
apt -y install nginx google-chrome-stable links 

#-- Perl component --
apt -y install carton libmodule-cpanfile-perl

#-- DB service component --
apt -y install sqlite3 

#-- Printing components --
apt -y install cups 

#-- Touch screen components --
apt -y install xinput xinput-calibrator

#-- Other for admining --
apt -y install mc build-essential

#-- for shared dir --
# - samba
apt install -y smbclient cifs-utils 

#++++++++++++++++++++++++++++++++++++++++++
echo "All components install complete.\n"
read -p "Starting service section deployment? (y/n):" -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	echo 'install sript interrupt!'
	            exit 1
		        fi


#-- deploy Perl env --
echo Start deployment perl environment? (y/n):
read quest
if [[$quest -eq 'y']]; then
carton install --cached --development
	fi
#-- Create mount point for smb shared dirs --
mkdir /mnt/smb_shared

#===== add system services config =====

#-- NGINX conf --
cp ./sys_conf/nginx/payterm  /etc/nginx/sites-available/
ln -s -f /etc/nginx/sites-available/payterm /etc/nginx/sites-enabled/payterm
systemctl restart ngnix

#-- SYSTEMD conf --
# copy services in systemd
cp ./sys_conf/systemd/chrome.service  /etc/systemd/user/
cp ./sys_conf/systemd/payterm.service /etc/systemd/system/
cp ./sys_conf/systemd/movefiles.service /etc/systemd/system/
cp ./sys_conf/systemd/movefiles.timer /etc/systemd/system/
# enabling services
sytemctl daemon-reload
systemctl --user enable chrome
systemctl enable payterm
systemctl enable movefiles
#-------------------------

