#!/usr/bin/env perl
#===============================================================================
#
#         FILE: payterm_print.pm
#
#        USAGE: ./payterm_print.pm  
#
#  DESCRIPTION: модуль работы с принтером для payterm_api
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 0.04
#      CREATED: 17.04.2020 11:44:24
#     REVISION: 25.06.2020 17:00:00
#===============================================================================

package payterm_print; #
our $VERSION = '0.04'; 

use strict;
use warnings;
use utf8;
use 5.010;
use Text::Template;
use POSIX qw(strftime);
use open qw(:std :utf8);
use JSON;
use Data::Printer;
#use Printer::ESCPOS;

sub new {
	my $class = shift;
	my $arg = shift;
	my $self = {
		printer_name => 'VKP80',   #'HP_LaserJet_Pro_MFP_M225rdn';
		device_lp => '/dev/usb/lp0',
	};
	bless ($self, $class);
	return $self;
};

my $check_tmpl = "check.tmpl";
my $incass_tmpl ="incass.tmpl";
my $INCASSO_NUMS = 100; # количество сохраняемых в базе инкассаций
#
#my $now_time= strftime "%d.%m.%Y T %X", localtime;

# Данные для файлов шаблона *.tmpl
my %templ=(
	INT_DEALER_NAME=>'ООО "Владлинк"',
	INT_DEALER_ADDRESS=>'пр-т Океанский, 125',
	INT_DEALER_INN=>'2540183433',
	INT_DEALER_PHONE=>'2-302-000',
	INT_POINT_ADDRESS=>'пр-т Океанский 125',
	INT_CURRENCY=>'руб',
	INT_RECIPIENT_NAME=>'ООО "Владлинк"',
	INT_RECIPIENT_INN=>'2540183433',
	#---- for print_incasso ----
	TERMINAL_NUMBER=>1,
	INCASSRECEIPTCOUNTER=>1,
	KASETTENUMBER=>0,
	#INCASSPERIODFROM=>'',
	#CURRENTDATETIME=>'',


);
#---- процедура проверки бумаги в принтере ----
sub paper_check{
	my $self = shift; 
	p $self;
	open(PRN,"+>$self->{device_lp}")|| 
		return {status=>0, description=>"device not open: $!"};
		my $send_str = pack('H*','100404');# ASC POS command: DLE EOT n
					       #             hex:  10 04 04
					       #                                            # Paper roll sensor status
		print(PRN $send_str);
		my $ret;
		read PRN, $ret, 1;
		p $ret;
		close PRN;
		my $status = unpack('C*', $ret);
		p $status;
		my $rulon_paper = $status&12;
		my $paper_in_feed = $status&96;
		my $descr_str;
			if ($rulon_paper){ 
				$descr_str.= "paper bulk ended ";
			} 
			else { 
				$descr_str.= "paper bulk full ";
			}
			if ($paper_in_feed){ 
				$descr_str.= ", paper out of feed";
			} 
			else {
				$descr_str.= ", paper in feed";
			}
				if ($paper_in_feed){
					return {status=>0, result=>$status, description=>$descr_str}
				}
				else {
				 return {status=>1, result=>$status, description=>$descr_str}
				}
}
#----------------------------------------------
#---- процедура печати чека клиенту -------
sub print_escrow {
	(my $self, $templ{SESSNUM}, $templ{DOGOVOR}, $templ{AMOUNTALL}) = @_;
	my $template = Text::Template->new(
		SOURCE => "$check_tmpl",
		ENCODING => 'UTF-8',
		) or die "Couldn't construct template: $Text::Template::ERROR";
		$templ{DATETIME} = strftime "%d.%m.%Y T %X", localtime;
		my $result = $template->fill_in(HASH => \%templ);
		p $result;
			if (defined $result) { 
				print $result;
				open (FH,'>','print_check.tmp') or die "sheff vseo propalo: $!";
				print FH $result;
				close FH;
				#my @strings = split ('\n', $result);
				say $self->{printer_name};
				say $$;
				system ("cancel -a"); # очищаем очередь печати на случай, если бумаги в принтере нет
				system ("lp -s -o media=om_x-80-mmy-160-mm_75.85x109.81mm -d $self->{printer_name}  print_check.tmp" );
				system ("rm  print_check.tmp" );
				#open(PH, "lp -s -o page-bottom=2 -o page-top=2 -d $self->{printer_name}  print_check.tmp |" ); # печать на принтер
				#close PH;
				

				#---- Секция печати -----
				#				my $device = Printer::ESCPOS->new(
				#				my $				driverType => 'File',
				#				my $				deviceFilePath =>$self->{device_lp},
				#				my $				#								vendorId   => 0x0dd4,
				#				my $				#								productId  => 0x015d,
				#				my $				#								endPoint   => 0x02,
				#				my $			);
				#				my $			foreach (@strings){
				#				my $			$device->printer->text($result);
				#				my $			}
							#$device->printer->printAreaWidth( 255 );
				#			$device->printer->font('a'); #a, b, c
				#			$device->printer->bold(1);
				#			$device->printer->fontHeight(1); # 0-7
				#			$device->printer->fontWidth(1); # 0-7
				#			$device->printer->charSpacing(5); #0-255
				#			$device->printer->lineSpacing($spacing); #0-255
				#       	$device->printer->cutPaper( feed => 0 );
				#			$device->printer->print();
				#			$device->printer->printerStatus;
				return {status=>1,  description=>"template printed"};			
				}
			else { 
				die "Couldn't fill in template: $Text::Template::ERROR";
				return {status=>0, description=>"Couldn't fill in template: $Text::Template::ERROR"};
				}
}

#------------------------------------------
#---- процедуры для инкассации ------------
#------------------------------------------

#---- процедура печати инкассо чека -------
sub print_incasso {           # основная инкассаторская функция с выходом в API
	my ($self, $db) = @_;
	my $new_report = check_incassed($db); # 0 - нет свежих значений по инкассации, 1 - свежий отчёт
	#----- забираем из базы последний отчёт -----
	my $sth = $db->prepare("SELECT incasso_id, curr_dtime, from_dtime, bills_json  FROM incasso_reports ORDER BY incasso_id DESC LIMIT 1"); # забираем последний отчёт
	my $rv = $sth->execute(); # or die $DBI::errstr;
		while(my @row = $sth->fetchrow_array()) {  # массив принятых купюр для инкассации
		$templ{INCASSRECEIPTNUMBER} = $row[0];
		$templ{CURRENTDATETIME} = $row[1];
		$templ{INCASSPERIODFROM} = $row[2];
		$templ{bill_nums} = decode_json $row[3];
		}	
		until ($templ{bill_nums}) { return {status=>0, description=>"no data for template"};}
	p %templ;
	#--------------------------------------------
	my $template = Text::Template->new(
		SOURCE => "$incass_tmpl",
		ENCODING => 'UTF-8',
		) or die "Couldn't construct template: $Text::Template::ERROR";
		my $result = $template->fill_in(HASH => \%templ);
			if (defined $result) { 
				print $result;
				open (FH,'>','print_incasso.tmp') or die "sheff vseo propalo: $!";
				print FH $result;
				close FH;
				system ("cancel -a"); # очищаем очередь печати на случай, если бумаги в принтере нет
				system ("lp -s -o media=om_x-80-mmy-160-mm_75.85x159.81mm -d $self->{printer_name}  print_incasso.tmp" );
				system ("rm  print_incasso.tmp" );
				#`lp -o page-bottom=2 -o page-top=2 -d $self->{printer_name}  $result`; # печать на принтер
				return {status=>1, description=>"template printed"};			
				}
			else { 
				die "Couldn't fill in template: $Text::Template::ERROR";
				return {status=>0, description=>"Couldn't fill in template: $Text::Template::ERROR"};
				}

}
#--------------------------------------------------------------------------------------

#---- внутренняя процедура проверки и подготовки отчёта по инкассации --------------
sub check_incassed{    # проверка таблицы sessions на наличие неинкассированнных значений
	my $db= shift;
	my $sth = $db->prepare("SELECT id, time, bill FROM sessions WHERE sess_begin_end='' AND incassed_at IS NULL ORDER BY bill");
	my $rv = $sth->execute() or die $DBI::errstr;
	my @incasso_arr; # двумерный массив данных
	my %bill_nums; # хэш купюра - количество 
		while(my @row = $sth->fetchrow_array()) {  # массив принятых купюр для инкассации
			$bill_nums{$row[2]}=0; # подготавливаем хэш купюр - инпициализируем 
			push(@incasso_arr, \@row); # сохраняем значения запроса в массив
		}
			until (@incasso_arr) { return 0;}
		foreach (@incasso_arr){
			$bill_nums{$_->[2]}+=1; # подсчитываем количество каждой банкноты
		}
	my $json = encode_json \%bill_nums; 
	#--------------------------------------------------------------------------------------
	$sth = $db->prepare("SELECT time FROM sessions WHERE sess_begin_end='' AND incassed_at IS NULL ORDER BY id ASC LIMIT 1"); # получаем from_dtime
	$rv = $sth->execute(); # or die $DBI::errstr;
	my @from_dtime = $sth->fetchrow_array(); # получаем значение времени from 
		#---- заносим результат в базу ----
		$db->do("INSERT INTO incasso_reports (curr_dtime, from_dtime, bills_json) VALUES (datetime('now','localtime'), '$from_dtime[0]', '$json' )" );
		#---- маркируем инкассированные результаты в базе----
		foreach (@incasso_arr){
			$db->do("UPDATE sessions SET incassed_at = '$from_dtime[0]' WHERE id='$_->[0]'");
		}
		cleardb($db,$from_dtime[0]);
return 1;
}
#------------------------------------------------------------------------

#---- процедура очистки базы данных - очищает данные за предыдущий период ----
sub cleardb {
	my ($db, $time) = @_; #
	say "$time to time deleted";
	my $sth = $db->do("DELETE FROM operations_log WHERE time < '$time'"); # очищаем таблицу логов операции за предыдущий период
	$sth = $db->do("DELETE FROM sessions WHERE time < '$time'"); # очищвем платёжные сессии за предыдущий период
	$sth = $db->prepare("SELECT COUNT(*) as count FROM incasso_reports"); # считаем количество инкассаторских отчётов
	my $rv = $sth->execute() or die $DBI::errstr;
	my @row;	
	while( @row = $sth->fetchrow_array()) {  # массив принятых купюр для инкассации
	}
		if ($row[0]>$INCASSO_NUMS){
			my $zz = $row[0]-$INCASSO_NUMS;
			$sth = $db->do("DELETE FROM incasso_reports WHERE ORDER BY incasso_id ASC LIMIT '$zz'");
		}

}

1; # module end



