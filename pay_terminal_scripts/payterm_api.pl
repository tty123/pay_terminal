#!/usr/bin/env perl
#===============================================================================
#
#         FILE: payterm_api.pl
#
#        USAGE: ./payterm_api.pl  
#
#  DESCRIPTION: API сервер для работы с валидатором и принтером
#               протокол http, формат данных JSON
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 0.04
#      CREATED: 06.04.2020 10:51:50
#     REVISION: 19.06.2020 10:40:50
#===============================================================================

use strict;
use warnings;
use utf8;

use JSON;
use AnyEvent::HTTPD;
use Data::Printer;
use DBI;
use XML::Simple;
use POSIX qw(strftime);

use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use CCNET; # подключаю свой модуль
use payterm_print; # модуль печати pos принтера

#---- initial config ----

my %config = %{do "payterm.conf"}; #загружаем конфиг из файла

my $PORT= $config{port}; # порт на котором работает API сервер
my $VLD_device = $config{vld_device}; # файл устройства валидатора
my $PRN_name = $config{prn_name};
my $PRN_device = $config{prn_device}; # файл устройства принтера
my $nom_en = $config{nom_en}; # битовая последовательность включения номиналов купюр
my $esc_en = $config{esc_en}; # битовая последовательность включения купюр в счёт
my $dbfile = $config{dbfile}; # файл БД
my $xml_path= $config{xml_path}; # папка для xml файлов пользовательских сессий
my $INCAPASS = $config{password}; # пароль для инкассации терминала
$| = 1;

#---- session vars ----
my $SESSNUM; # номер сессии
my $DOGOVOR; # номер договора
my $PAYMENT_TIME; # время начала формирования платежа
my $SESSION_CHECK=0; # 0 -  платёжная сессия открыта, 1 - закрыта

#----------------------------------------------------------
my $httpd = AnyEvent::HTTPD->new(port => $PORT);

#---- подготовка БД ----
my $db = DBI->connect("dbi:SQLite:dbname=$dbfile","","");#  or die($db->errstr);
DBI->trace(1); 

#---- выполняем подготовку валидатора ------
my $pcash = CCNET-> new();  # подключаем объект валидатора
$pcash->{device}=$VLD_device;
p $pcash;
my $pinca = payterm_print->new();  # подключаем объект печати отчётов и инкассации
$pinca->{device_lp}=$PRN_device; 
$pinca->{printer_name}=$PRN_name; 
p $pinca;
$pcash->set_bills($nom_en, $esc_en) or die "module CCNET unavailable"; 
#---- инициализация ----
my $result = $pcash->init_power_on;
my $json = encode_json($result);
	if ($result->{status}){
		$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init on API start','1','$json' )"); #logging
		print "validator initialized\n";
	}
	else{
		if (($result->{response} eq "-1")||($result->{response} eq "-2")){
		$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init on API start','0','$json' )"); #logging
		print "validator error connection on start API\n";
		}
		else {
		$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init on API start ','0','$json' )"); #logging
		print "validator error on start API\n";
		}	}

print "pay terminal API running!\n";

#---- включение http сервера ----
$httpd->reg_cb(
	request => sub {
		my ($httpd, $req) = @_;
		my $url = $req->url;
		my $mt = $req->method;
		my $hds = $req->headers;
		my %dt = $req->vars; # хэш значений формы
		my %headers = ( 
			'Access-Control-Allow-Origin'=>'*', 
			'Access-Control-Allow-Methods'=>'POST,GET,OPTIONS', 
			'Content-Type' =>'application/json',
		);
		#p $url;
		if (($url eq '/term') && ($mt eq 'POST')) {
			if ($dt{api} eq 'payterm') {
				#--------------------------- блок функций API --------------------------------------
				if ($dt{cmd} eq 'set_bills') {
					my $bl = $pcash->set_bills($dt{nom_en}, $dt{esc_en});
					if ($bl) {
						my $json = '{"status":1}';
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'set_bills','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
						}
					else {
						my $json = '{"status":0}';
						$req->respond([200,'',\%headers, $json]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'set_bills','0','$json' )"); #logging
					}
				} 
				elsif ($dt{cmd} eq 'init_power_on'){ 	# [] инициализация валидатора
					my $result = $pcash->init_power_on;
					my $json = encode_json($result);
					if ($result->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init_power_on','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init_power_on','0','$json' )"); #logging
						}
						else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init_power_on','0','$json' )"); #logging
						}
					}
				}	
				elsif ($dt{cmd} eq 'check_init'){ 	# проверка инициализации валидатора в модуле
					my $result = $pcash->check_init;
					if (defined $result){
						my $json = encode_json($result);
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'check_init','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([500,'',\%headers, '{"error":"module CCNET unavailable"}']);
						}
				}	
				elsif ($dt{cmd} eq 'escrow_on'){ 	# []
					my $result = $pcash->escrow_on;
					$DOGOVOR =''; # сбрасываем номер договора
					$SESSION_CHECK = 0; # сбрасываем сигнал завершения плвтёжной сессии для create_xml_fil
					#---- логгирование сессии ----
					$SESSNUM = time;               # session log 
					$PAYMENT_TIME = strftime "%d.%m.%Y %X", localtime;
					session_log ('','BEGIN');  # session log
					#-----------------------------
					my $json = encode_json($result);
					if ($result->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'escrow_on','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}	
					else{
						if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'escrow_on','0','$json' )"); #logging
						}
						else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'escrow_on','0','$json' )"); #logging
						}
					}
				}	
				elsif ($dt{cmd} eq 'bulk_escrow_on'){ 	# блочная функция включения на приём банкнот, на вход номер договора 
					if  ($dt{dogovor}=~/^\d+$/) {
						$DOGOVOR = $dt{dogovor}; # сохраняем номер счёта пользователя в переменной
						#---- проверка инициализации ----
						my $result = $pcash->get_poll;
						if (($result->{response} ne '14')&&($result->{response} ne '19')){
							my $result = $pcash->init_power_on;
							my $json = encode_json($result);
							if ($result->{status}){
								#$req->respond([200,'',\%headers, $json ]);
								$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init_power_on','1','$json' )"); #logging
								print "command $dt{cmd} accepted\n";
							}
							else{
								if (($result->{response} eq "-1")||($result->{response} eq "-2")){
								$req->respond([500,'',\%headers, $json ]);
								$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init_power_on','0','$json' )"); #logging
								print "validator error connection:  $dt{cmd}\n";
								}
								else {
								$req->respond([200,'',\%headers, $json ]);
								$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'init_power_on','0','$json' )"); #logging
								print "validator error:  $dt{cmd}\n";
								}
							}
						}
						#--------------------------------
						$result = $pcash->escrow_on;
						$SESSION_CHECK = 0; # сбрасываем сигнал завершения платёжной сессии для create_xml_fil
						if ($result->{status}){
						   $result = $pcash->get_approved_nominals;
							my $json = encode_json($result);
							if ($result->{status}){
								$req->respond([200,'',\%headers, $json ]);
								$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_on','1','$json' )"); #logging
								#---- логгирование сессии ----
								$SESSNUM = time; 		# session log 
								$PAYMENT_TIME = strftime "%d.%m.%Y %X", localtime;
								session_log ('','BEGIN');   # session log 
								#-----------------------------
								print "command $dt{cmd} accepted\n";
							}
							else{
								$req->respond([500,'',\%headers, $json ]);
								$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_on','0','$json' )"); #logging
							}
						}
						else{
							my $json = encode_json($result);
							if (($result->{response} eq "-1")||($result->{response} eq "-2")){
							$req->respond([500,'',\%headers, $json ]);
							$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_on','0','$json' )"); #logging
							}
							else {
							$req->respond([200,'',\%headers, $json ]);
							$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_on','0','$json' )"); #logging
							}
						}
					}
					else {
					my $json = '{"status":0,"description":"invalid dogovor number"}';
					$req->respond([200,'',\%headers, $json ]);
					$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_on','0','$json' )"); #logging
					}
				}	
				elsif ($dt{cmd} eq 'bulk_get_bill'){ 	# блочная функция на приём банкнот. 
					my $result = $pcash->bill_stack_pos;
					if ($result->{status}){
						my $result = $pcash->bill_stack_add;
							if ($result->{status}){
								my $json = encode_json($result);
								$req->respond([200,'',\%headers, $json ]);
								$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_get_bill','1','$json' )"); #logging
								#---- логгирование сессии ----
								session_log ($result->{result},''); # session log
								#-----------------------------
								print "command $dt{cmd} accepted\n";
							}
							else{
								if (($result->{response} eq "-1")||($result->{response} eq "-2")){
								my $json = encode_json($result);
								$req->respond([500,'',\%headers, $json ]);
								$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_get_bill','0','$json' )"); #logging
								}
								else {
								my $json = encode_json($result);
								$req->respond([200,'',\%headers, $json ]);
								$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_get_bill','0','$json' )"); #logging
								}
							}
					}
					else{
						if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						my $json = encode_json($result);
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_get_bill','0','$json' )"); #logging
						}
						else {
						my $json = encode_json($result);
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_get_bill','0','$json' )"); #logging
						}
					}
				}	
				#-------------- здесь закончил 19.05.2020 ------------
				elsif ($dt{cmd} eq 'bulk_escrow_off'){ 	#  result 0 - всё плохо, 1 - всё ок, 2 - ок, нет бумаги в подаче, 3 - ок, нет бумаги в рулоне,
									#  4 - ок, нет бумаги нигде
					my $result = $pcash->escrow_off;
					$result = $pcash->get_escrow_sum; # забираем сумму сессии
					my $json = encode_json($result);
					if ($result->{status}){
						if ($result->{result}){
							$SESSION_CHECK = 1; # сигнал закрытия платёжной сессии для create_xml_file
							create_xml_file(); # сохраняем файл платёжной сессии на диск 
							#my $paper_res = {status=>1, result=>'96',description=>'test'};# $pinca->paper_check();
							#$result->{printer} = $paper_res;
							#if ($paper_res->{status}){
									my $printer = $pinca->print_escrow($SESSNUM, $DOGOVOR, $result->{result});
									$result->{template} = $printer;
									my $json = encode_json($result);
									$req->respond([200,'',\%headers, $json ]);
									$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_off','1','$json' )"); #logging
									#}else {
									#$json = encode_json($result);
									#$req->respond([200,'',\%headers, $json ]);
									#$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_off','1','$json' )"); #logging
									#}
						}
						else{
							$req->respond([200,'',\%headers, $json ]);
							$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_off','1','$json' )"); #logging
						}
						#---- логгирование сессии ----
						session_log ('','END');  # session log
						#-----------------------------
						print "command $dt{cmd} accepted\n";
					}
					else{
						if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_off','1','$json' )"); #logging
						}
						else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bulk_escrow_off','1','$json' )"); #logging
						}
					}
				}
				#-------------------------------------------------------
				elsif ($dt{cmd} eq 'create_xml_file'){ 	# API функция сохранения файла платежа
					my $xml=create_xml_file();
					print "xml worked!!!!!\n";
					my $json = encode_json($xml);
					if  ($xml->{status}) {
						$req->respond([200,'',\%headers, $json ]);
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([200,'',\%headers, $json ]);
					}

				}	
				elsif ($dt{cmd} eq 'set_dogovor'){ 	# []
					if  ($dt{dogovor}=~/^\d+$/) {
						$DOGOVOR = $dt{dogovor}; # сохраняем номер счёта пользователя в переменной
						my $json = qq/{"status":1,"description":"valid user escrow number","result":"$dt{dogovor}"}/;
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'set_user_escrow','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						my $json = qq/{"status":0,"description":"invalid user escrow number","result":"$dt{dogovor}"}/;
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'set_user_escrow','0','$json' )"); #logging
					}

				}	
				elsif ($dt{cmd} eq 'escrow_off'){ 	# []
					my $result = $pcash->escrow_off;
					my $json = encode_json($result);
					if ($result->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'escrow_off','1','$json' )"); #logging
						$SESSION_CHECK = 1; # сигнал закрытия платёжной сессии для create_xml_file
						#---- логгирование сессии ----
						session_log ('','END');  # session log
						#-----------------------------
						print "command $dt{cmd} accepted\n";
					}
					else{
						if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'escrow_off','0','$json' )"); #logging
						}
						else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'escrow_off','0','$json' )"); #logging
						}
					}
				}	
				elsif ($dt{cmd} eq 'get_nominals_table'){ 	# [] 
					my $result = $pcash->get_nominals_table;
					my $json = encode_json($result);
					if ($result->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_nominals_table','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_nominals_table','0','$json' )"); #logging
					}

				}	
				elsif ($dt{cmd} eq 'get_approved_nominals'){ 	# [] 
					my $result = $pcash->get_approved_nominals;
						my $json = encode_json($result);
					if ($result->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_approved_nominals','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_approved_nominals','0','$json' )"); #logging
					}

				}	
				elsif ($dt{cmd} eq 'bill_stack_pos'){ 	# [] 
					my $result = $pcash->bill_stack_pos;
					my $json = encode_json($result);
					if ($result->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_pos','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_pos','0','$json' )"); #logging
						}
						else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_pos','0','$json' )"); #logging
						}
					}

				}	
				elsif ($dt{cmd} eq 'bill_stack_add'){ 	# []
					my $result = $pcash->bill_stack_add;
					my $json = encode_json($result);
					if ($result->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_add','1','$json' )"); #logging
						#---- логгирование сессии ----
						session_log ($result->{result},''); # session log
						#-----------------------------
						print "command $dt{cmd} accepted\n";
					}
					else{
						if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_add','0','$json' )"); #logging
						}
						else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_add','0','$json' )"); #logging
						}
					}

				}	
				elsif ($dt{cmd} eq 'bill_stack_return'){ 	# [] 
					my $result = $pcash->bill_stack_return;
					my $json = encode_json($result);
					if ($result->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_return','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_return','0','$json' )"); #logging
						}
						else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'bill_stack_return','0','$json' )"); #logging
						}
					}

				}	
				elsif ($dt{cmd} eq 'get_escrow_sum'){ 	# \ 
					my $result = $pcash->get_escrow_sum;
					my $json = encode_json($result);
					if ( $result->{status} ne "0" ){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_escrow_sum','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_escrow_sum','0','$json' )"); #logging
					}

				}	
				elsif ($dt{cmd} eq 'get_escrow'){ 	# 
					my $result = $pcash->get_escrow;
					my $json = encode_json($result);
					if ( $result->{status} ne "0" ){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_escrow','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_escrow','0','$json' )"); #logging
					}
				}	
				elsif ($dt{cmd} eq 'get_identification'){ 	# 
					my $result = $pcash->get_identification;
					my $json = encode_json($result);
					if ( $result->{status} ne "0" ){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_identification','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_identification','0','$json' )"); #logging
					}
				}	
				elsif ($dt{cmd} eq 'get_poll'){ 	# 
					my $result = $pcash->get_poll;
					my $json = encode_json($result);
					if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_poll','0','$json' )"); #logging
					}
					else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_poll','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
				}	
				elsif ($dt{cmd} eq 'get_request_statistics'){ 	# 
					my $result = $pcash->get_request_statistics;
					my $json = encode_json($result);
					if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_request_statistics','0','$json' )"); #logging
					}
					else {
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'get_poll','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
				}	
				elsif ($dt{cmd} eq 'check_incasso'){ 	# проверка открытия кассеты валидатора, для перехода в режим инкассации 
					my $result = $pcash->get_poll;
					my $json = encode_json($result);
					if (($result->{response} eq "-1")||($result->{response} eq "-2")){
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'check_incasso','0','$json' )"); #logging
					}
					else {
						if ($result->{response} eq "42"){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'check_incasso','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
						}
						else{
						$result->{status}=0;
						my $json = encode_json($result);
						$req->respond([200,'',\%headers, $json ]);
						#$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'check_incasso','0','$json' )"); #logging заблокировал, так как забивает базу кучей ненужных строк
						}
					}
				}	
				elsif ($dt{cmd} eq 'print_escrow'){ 	# Заглушка функции печати 
					my $result = $pcash->get_escrow_sum; # забираем сумму сессии
					my $printer = $pinca->print_escrow($SESSNUM, $DOGOVOR, $result->{result});
					my $json = encode_json($printer);
					if ($printer->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'print_escrow','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'print_escrow','0','$json' )"); #logging
					}
				}	
				elsif ($dt{cmd} eq 'print_incasso'){ 	# Заглушка функции печати 
					# $dt{JSON_BILLS_INCASSO};
					if ($dt{incapass} eq $INCAPASS){
						#my $test_json='{"INCASSPERIODFROM":"10.02.2020","bills_data":{"100":30,"1000":23,"50":112},"CURRENTDATETIME":"21.04.2020"}';
						my $printer = $pinca->print_incasso($db);
						my $json = encode_json($printer);
						p $printer;
						if ($printer->{status}){
							$req->respond([200,'',\%headers, $json ]);
							$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'print_incasso','1','$json' )"); #logging
							print "command $dt{cmd} accepted\n";
						}
						else{
							$req->respond([500,'',\%headers, $json ]);
							$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'print_incasso','0','$json' )"); #logging
						}
					}else{
						$json = '{"status":0,"description":"invalid password"}';
						$req->respond([403,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'print_incasso','0','$json' )"); #logging
					}
				}	
				elsif ($dt{cmd} eq 'paper_check'){ 	# Проверка бумаги в принтере 
					my $printer = $pinca->paper_check();
					my $json = encode_json($printer);
					if ($printer->{status}){
						$req->respond([200,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'paper_check','1','$json' )"); #logging
						print "command $dt{cmd} accepted\n";
					}
					else{
						$req->respond([500,'',\%headers, $json ]);
						$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'paper_check','0','$json' )"); #logging
					}
				}	

				#-------------------- конец блока API функций --------------------------------------
				
				else { 
					my $json = '{"error":"cmd"}';
					$req->respond([400,'',\%headers, $json ]); 
					$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'http server','0','$json' )"); #logging
				};
			}
			else { 
				my $json = '{"error":"api"}';
				$req->respond([400,'',\%headers, $json]);
				$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'http server','0','$json' )"); #logging
			};
		}
		else { 
			my $json = '{"error":"url"}';
			$req->respond([400,'',\%headers, $json]);
			$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'http server','0','$json' )"); #logging
		};
	},
);

$httpd->run; # запуск
$db->disconnect(); # закрываем базу

#---- функция сохранения сессионной статистики ----
sub session_log{
	my ($bill, $sess_begin_end)=@_;
	if (!defined $SESSNUM){$SESSNUM=''};
	if (!defined $DOGOVOR){$DOGOVOR=''};
	my $ref = $pcash->get_escrow_sum;
	my $sum = $ref->{result};
	$db->do( "INSERT INTO sessions (time, sessnum, dogovor, bill, sum, sess_begin_end) VALUES ( datetime('now','localtime'),'$SESSNUM','$DOGOVOR','$bill','$sum','$sess_begin_end' )"); #logging
}
#---------------------------------------
#---- Функция формирования XML файла для аккаунта ----
sub create_xml_file{
	my $ref = $pcash->get_escrow_sum;
	my $sum = $ref->{result};
		if (!($sum&&$DOGOVOR&&$SESSNUM&&$SESSION_CHECK)){
			my %str = (
				status=>0,
				description=>"file not save, no payment or session not closed",
			);
			$json = encode_json(\%str);
			$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'create_xml_file','0','$json' )"); #logging
			return \%str;
		}
	my %hash_ref = ( 
		root=>{
			fields=>{
				field100=>"$DOGOVOR",
				AMOUNT_ALL=>"$sum",
				#			test=>'проверка',
			},
			payment_create_dt=>"$PAYMENT_TIME",
		}
	);
	my $file = 'ext-'.$SESSNUM.$DOGOVOR.'.pkt';
	my $path_file=$xml_path.$file;
	my $check=1;
	my %str = (
		status=>1,
		description=>"payment saved on disk, file: $file",
	);
	$json = encode_json(\%str);

	open my $fh, '>:encoding(windows-1251)', $path_file or $check = 0;
		if ($check) {
		XMLout(\%hash_ref, OutputFile => $fh, NoAttr => 1, KeepRoot=>1, XMLDecl=>'<?xml version="1.0" encoding="windows-1251" standalone="yes"?>');
		close $fh;
		$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'create_xml_file','1','$json' )"); #logging
		return \%str;
		}
		else {
		%str = (
			status=>0,
			description=>"open($path_file): $!",
		); 
		$json = encode_json(\%str);
		$db->do( "INSERT INTO operations_log (time,function,status,json) VALUES ( datetime('now','localtime'),'create_xml_file','0','$json' )"); #logging
		return \%str; 
		}
}
#-----------------------------------------------------


print "ok";
