#
#===============================================================================
#
#         FILE: CCNET.pm
#
#  DESCRIPTION: This is realization CashCodeNet protocol for working
#               with CashCode validators (03h). 
#
# Author Tolmachev V.N. <>
# Version 0.04
# Copyright (C) 2020 Tolmachev V.N. <>
# Modified On 2020-05-26 12:29
# Created  2020-03-10 14:27
#
#===============================================================================

package CCNET;
use strict;
use warnings;
use Device::SerialPort;
use Data::Printer;
use 5.010;
use constant DEBUG=>0; # включение-выключение отладочной информации модуля 

#our @ISA = qw(Exporter);
#our %EXPORT_TAGS = ( 'all' => [ qw() ] );
#our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );
#our @EXPORT = qw();
our $VERSION = '0.04';
#require Exporter;
#use AutoLoader qw(AUTOLOAD);


sub new
{
    my $class = shift;
    my $arg = shift;
    my $self = {
	    device => "/dev/ttyUSB0",
	    baudrate => "9600",
	    parity =>"none",
	    databits => "8",
	    stopbits => "1",
	    ccnet_adr => '03',
	
    };

    bless($self, $class);
    return $self;
}


my %CMD=( 
	'ack' =>'00',
	'nack'=>'ff',
	'reset' => '30',
	'get_status' => '31',
	'set_security' => '32', # set 3 bytes
	'poll' => '33',
	'enable_bill_types' => '34', # set 6 bytes
	'stack' => '35',
	'return' => '36',
	'identification' => '37',
	'hold' => '38', #эта команда позволяет контроллеру удерживать купюроприёмник в Escrow режиме в течение 10 с.
	'set_barcode_parameters' => '39', # set 2 bytes
	'extract_barcode_data' => '3A',
	'get_bill_table' => '41',
	'download' => '50',
	'get_crc32_of_the_code' => '51',
	'request_statistics' => '60',
);

my %CMDinv= reverse %CMD; #debug

my %response = (
	'0' => 'nack',
	'-1'=> 'connection error ',
	'-2' => 'CRC error',
	'00' => 'ack',
	'ff' => 'nack',
	'10' => 'Power Up',
	'11' => 'Power Up with Bill in Validator',
	'12' => 'Power Up with Bill in Stacker',
	'13' => 'Initialize',
	'14' => 'Idling',
	'15' => 'Accepting',
	'17' => 'Stacking',
	'18' => 'Returning',
	'19' => 'Unit Disabled',
	'1a' => 'Holding',
	'1b' => 'Device Busy',
	'1c60'=> 'Rejecting due to Insertion',
	'1c61'=> 'Rejecting due to Magnetic',
	'1c62'=> 'Rejecting due to Remained bill in head',
	'1c63'=> 'Rejecting due to Multiplying',
	'1c64'=> 'Rejecting due to Conveying',
	'1c65'=> 'Rejecting due to Identification1',
	'1c66'=> 'Rejecting due to Verification',
	'1c67'=> 'Rejecting due to Optic',
	'1c68'=> 'Rejecting due to Inhibit',
	'1c69'=> 'Rejecting due to Capacity',
	'1c6a'=> 'Rejecting due to Operation',
	'1c6c'=> 'Rejecting due to Length',
	'1c6d'=> 'Rejecting due to UV',
	'1c92'=> 'Rejecting due to unrecognized barcode',
	'1c93'=> 'Rejecting due to incorrect number of characters in barcode',
	'1c94'=> 'Rejecting due to unknown barcode start sequence',
	'1c95'=> 'Rejecting due to unknown barcode stop sequence',
	#	'1d00'=> 'DISPENSING – B2B moves the bill(s) from recycling cassette to dispenser',
	#	'1d01'=> 'DISPENSING – B2B remains in this state until customer take the bill(s) from dispenser',
	#	'1e00'=> 'UNLOADING – B2B is moving the bill(s) from recycling cassette to drop cassette',
	#	'1e01'=> 'UNLOADING – B2B is moving the bill(s) from recycling cassette to drop cassette. Number of bills requested is more than the number of bills in the cassette',
	#	'21' => 'Setting Type Cassete',
	#	'25' => 'Dispensed',
	#	'26' => 'Unloaded',
	#	'28' => 'Invalid Bill Number',
	#	'29' => 'Set Cassete Type',
	'30' => 'Illegal Command', # используется в ответе, если текущая комманда не соответствует ожиданию  периферии
	'41' => 'Drop Cassette Full',
	'42' => 'Drop Cassette out of position',
	'43' => 'Validator Jammed',
	'44' => 'Drop Casserte Jammed', 
	'45' => 'Cheated',
	'46' => 'Pause',
	'4750' => 'Stack Motor Failure',
	'4751' => 'Transport Motor Speed Failure',
	'4752' => 'Transport Motor Failure',
	'4753' => 'Aligning Motor Failure',
	'4754' => 'Initial Cassette Status Failure ',
	'4755' => 'Optic Canal Failure ',
	'4756' => 'Magnetic Canal Failure ',
	'475f' => 'Capacitance Canal Failure ',
	'8000' => 'Escrow Position',
	'8001' => 'Escrow Position',
	'8002' => 'Escrow Position',
	'8003' => 'Escrow Position',
	'8004' => 'Escrow Position',
	'8005' => 'Escrow Position',
	'8006' => 'Escrow Position',
	'8007' => 'Escrow Position',
	'8008' => 'Escrow Position',
	'8009' => 'Escrow Position',
	'800a' => 'Escrow Position',
	'800b' => 'Escrow Position',
	'800c' => 'Escrow Position',
	'800d' => 'Escrow Position',
	'800e' => 'Escrow Position',
	'800f' => 'Escrow Position',
	'8010' => 'Escrow Position',
	'8011' => 'Escrow Position',
	'8012' => 'Escrow Position',
	'8013' => 'Escrow Position',
	'8014' => 'Escrow Position',
	'8015' => 'Escrow Position',
	'8016' => 'Escrow Position',
	'8017' => 'Escrow Position',
	'8100' => 'Bill stacked',
	'8101' => 'Bill stacked',
	'8102' => 'Bill stacked',
	'8103' => 'Bill stacked',
	'8104' => 'Bill stacked',
	'8105' => 'Bill stacked',
	'8106' => 'Bill stacked',
	'8107' => 'Bill stacked',
	'8108' => 'Bill stacked',
	'8109' => 'Bill stacked',
	'810a' => 'Bill stacked',
	'810b' => 'Bill stacked',
	'810c' => 'Bill stacked',
	'810d' => 'Bill stacked',
	'810e' => 'Bill stacked',
	'810f' => 'Bill stacked',
	'8110' => 'Bill stacked',
	'8111' => 'Bill stacked',
	'8112' => 'Bill stacked',
	'8113' => 'Bill stacked',
	'8114' => 'Bill stacked',
	'8115' => 'Bill stacked',
	'8116' => 'Bill stacked',
	'8117' => 'Bill stacked',
	'8200' => 'Bill returned',
	'8201' => 'Bill returned',
	'8202' => 'Bill returned',
	'8203' => 'Bill returned',
	'8204' => 'Bill returned',
	'8205' => 'Bill returned',
	'8206' => 'Bill returned',
	'8207' => 'Bill returned',
	'8208' => 'Bill returned',
	'8209' => 'Bill returned',
	'820a' => 'Bill returned',
	'820b' => 'Bill returned',
	'820c' => 'Bill returned',
	'820d' => 'Bill returned',
	'820e' => 'Bill returned',
	'820f' => 'Bill returned',
	'8210' => 'Bill returned',
	'8211' => 'Bill returned',
	'8212' => 'Bill returned',
	'8213' => 'Bill returned',
	'8214' => 'Bill returned',
	'8215' => 'Bill returned',
	'8216' => 'Bill returned',
	'8217' => 'Bill returned',
);

# %VLD_state.
# После выполнения команд, принимающих данные,  все важные данные: информация о валидаторе, таблицы купюр, массив счёта, общая сумма счёта,
# помещаются в данный хэш.
my %VLD_state = (	#  информация полученная с валидатора
	'bills_status' =>[],	# cmd_get_status 6 bytes
	#'get_status' =>'none',	# стандартная реакция на команду	
	'bill_table' =>[],	# вывод команды cmd_get_bill_table
	'bill_table_cnt' =>[],	# вывод команды cmd_get_bill_table
	'error' =>'',	#   ??????????????
	'escrow'=>[],	# массив принятых купюр
	'escrow_sum'=>0, # сумма принятых купюр
	'init'=>0, # валидатор не инициализирован в модуле CCNET
);


my $SYNC = '02';# 02h
#my $ADR = $self; #03h
my $POLYNOM = '8408';
my $SET_BILLS = '0038FC';   # по умолчанию аргумент "выбор банкнот" для команды cmd_enable_bill_types
my $SET_BILLS_SEC = '0038FC'; # по умолчанию аргумент "выбор усиленной проверки банкнот"  для команды cmd_enable_bill_types

#p $PortObj;
my $cmd_repeat = 5; # максимальное количество повторов команды, в случае получения NACK

#%%%%%%%%%%%%%%%%%%%%%%%%  Подготовка сообщений  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#---- SUB подготовка команды контроллера -----------------------------
sub cmd_message_prep {      
	my ($self, $CMD, $DATA) = @_;
	my $mess;
		if (!defined($DATA)){   # сообщение с командой 
			my $LNG = '06';	#06h
			my $ADR = $self->{ccnet_adr};
			my $CRC = $self->ccnet_crc16 ($SYNC.$self->{ccnet_adr}.$LNG.$CMD);
			$mess = $SYNC.$self->{ccnet_adr}.$LNG.$CMD.$CRC;
		}else{   # сообщение с командой и дополнительными данными   
			my $LNG = sprintf("%x", (6 + length($DATA)/2));
			if (length($LNG) eq '1'){$LNG='0'.$LNG;} # дополняем hex до hex-а
			my $CRC = $self->ccnet_crc16 ($SYNC.$self->{ccnet_adr}.$LNG.$CMD.$DATA);
			$mess = $SYNC.$self->{ccnet_adr}.$LNG.$CMD.$DATA.$CRC;
		}
	say '$mess='.$mess." )----------> \"\$CMD = $CMDinv{$CMD}\"" if DEBUG; #debug
	my $packed_mess = pack('H*', $mess);
return \$packed_mess;
}
#---------------------------------------------------------------------


#---- SUB response decode -----------------------------
sub response_decode {  # из сообщения забираем блок с данными
	my ($self, $decoded_str) = @_;
	my $CRCr = substr ($decoded_str,-4);
	my $DATASTR = substr ($decoded_str,0,-4);
	if ($CRCr ne ($self->ccnet_crc16($DATASTR))){ 
			return '-2'; # return CRC error
		} # Если CRC good, то разбираем статус
	 # Проверяем CRC в зависимоти рез - BAD CRC
return substr($decoded_str,6,-4);
}
#---------------------------------------------------------------------

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#---- SUB send-request message ---------------------------------------
=encoding  UTF8
=head1 sub send_mess
Процедура  send_mess
Описание:
	Отправляет hex строку в указанный последовательный интерфейс. 
	Принимает ответ от устройства и проверяет CRC. Если проверка
	проходит, то отдаётся результат, иначе команда повторяется
	заданное количество раз..
	
аргументы: 
	строка в hex
результат: 
	-1 - проблема с линией, CRC не совпадают.
	     Команда безуспешно повторилась несколько раз. 
	 0 - NACK устройство не подтверждает команду
	hex string - успешное получение данных (ack - если команда не получает данные)
=cut

sub send_mess{
my ($self, $mess_ref) = @_;
#my $Configuration_File_Name='conf';
	my $lng_mess = length($$$mess_ref);
	##p $lng_mess;
	#my $packed_mess = pack('H*', $$mess_ref);
	my $count_req = 0;
	my $PortObj=Device::SerialPort->new($self->{device}) || 
	#die "Can't open $self->{device}: $!\n";
	return "-1";
	$PortObj->baudrate($self->{baudrate});
	$PortObj->parity($self->{parity});
	$PortObj->databits($self->{databits});
	$PortObj->stopbits($self->{stopbits});
	#$PortObj-> write_settings || undef $PortObj;
	#$PortObj-> read_interval(10);
M1:
#	say 'sub send_mess('.unpack('H*', $$$mess_ref).')' if DEBUG; #debug
	my $count_out = $PortObj->write($$$mess_ref);
	##p $count_out;
		warn "write failed\n"     unless ($count_out);
		#print length($mess);
		warn "write incomplete\n" if ( $count_out != $lng_mess );
		#	p $PortObj;
		#~~~~~~~~~~ peripheral response 	
		$PortObj->read_char_time(0);     # don't wait for each character
		$PortObj->read_const_time(200); # 0,01 second per unfulfilled "read" call
		my ($count, $result) = $PortObj->read(255); # read 255 chars
		say 'response $result = '.unpack('H*',$result) if DEBUG; #debug
		p $result if DEBUG; #debug
		if ( $result eq ''){
			say "error connection!";	
			return -1; # нет ответа, проблема соединения или устройство не работает
			}
		$result = unpack('H*',$result);
		say "RX ok" if DEBUG; #debug
		p $result if DEBUG; #debug
		my $response_data = $self->response_decode($result);
			if (defined($response{$response_data})){		# отсеваем несуществующие значения хэша
				say 'send_mess $response_data = '.$response_data."  <----------( \"$response{$response_data}\"" if DEBUG; # debug
			#----- проверяем CRC ответа-------------------
				if ($response{$response_data} eq 'CRC error') {
					$count_req++; 
					# подтверждение неудачного получения данных
					my $mess = $self->cmd_message_prep($CMD{nack});
					$PortObj->write($$mess);
					return '-1' if $count_req ge $cmd_repeat; #  отдаём ошибку -1 если команда не проходит
					goto M1;
					}
			#----- проверяем NACK ответ, если устройство некорректно получило данные
				if ($response{$response_data} eq 'nack'){
					return 0 if $count_req >= $cmd_repeat; #  отдаём ошибку 0 если периферия непонимает или непринимает команду
					goto M1;
				}
			#----- отдаём, если всё нормально
					# подтверждение удачного получения данных
				if ($response{$response_data} ne 'ack'){
					my $mess = $self->cmd_message_prep($CMD{ack});
					#say "send ACK = ".unpack('H*', $$mess) if DEBUG; #debug
					$PortObj->write($$mess);
				}
			}
		#print "count = $count, result = $result\n";
		return $self->response_decode($result);
}
#---------------------------------------------------------------------


#---- SUB CRC16 -----------------------------------------------------------------
sub ccnet_crc16{               # принимаем строку в hex-ах
	my ($self, $InDataStr) = @_;
	my $POLYNOM = 33800;# = 8408h
	my @InData = unpack '(a[2])*',$InDataStr;
	my $sizeData=@InData;
	my $CRC=0;
		for (my $i=0; $i < $sizeData; $i++)
		{
		$CRC^=hex($InData[$i]);
			for (my $j=0; $j<8; $j++)
			{
				if ($CRC&1){
					$CRC>>=1; 
					$CRC^=$POLYNOM;
				}
				else{
					$CRC>>=1; 
				}
			}	
		}
				$CRC = sprintf("%x", $CRC); # переводим в hex, может получиться 3 символа вместо 4-[, тогда ->
				if (length($CRC) < 4){    # -> добавляем 0, если число получилось в 3 знака (наппр. при 02030619)
				$CRC ='0'.$CRC;
	}
			my @arr = unpack '(a[2])*', $CRC; 
			if ((defined $arr[0])&&(defined $arr[1])){
				return $arr[1].$arr[0]; # отдаем 2-х байтный CRC
			}
		return '';
}
#----------------------------------------------------------------------------------

#---- процедуры для работы с последовательностями-----
sub cmd_poll {		# запрос состояния периферии, в ответе статус
	my $self = shift;
	my $rsp = $self->cmd_message_prep($CMD{poll});
return  ($self->send_mess(\$rsp));
}

sub cmd_reset {		# сброс периферии, в ответе ACK
	my $self = shift;
	my $rsp = $self->cmd_message_prep($CMD{reset});
return  $self->send_mess(\$rsp);
}

sub cmd_stack {		# добавить вставленную купюру в кассету
	my $self = shift;
	my $rsp = $self->cmd_message_prep($CMD{stack});
return  $self->send_mess(\$rsp);
}

sub cmd_hold {		# удержание в режиме счёта купюр в течении 10 секунд
	my $self = shift;
	my $rsp = $self->cmd_message_prep($CMD{hold});
return  $self->send_mess(\$rsp);
}

sub cmd_return {	# вернуть вставленную купюру
	my $self = shift;
	my $rsp = $self->cmd_message_prep($CMD{return});
return  $self->send_mess(\$rsp);
}

sub set_bills{		# включаем и выключаем купюры для команды cmd_enable_bill_types
	my ($self, $bs_en, $bs_sec)=@_;
	if (($bs_en=~/....../)&&($bs_sec=~/....../)){
		$SET_BILLS = $bs_en;
		$SET_BILLS_SEC = $bs_sec;
		return 1; # успешно изменили переменные
		}
return 0;
}


sub cmd_enable_bill_types {		# разрешить-запретить приём банкнот, разрешить-запретить приём в режиме счёта
	my $self = shift;
	my $rsp;
		if (defined($SET_BILLS) && defined($SET_BILLS_SEC)){
			my $mess=$SET_BILLS.$SET_BILLS_SEC;
			$rsp = $self->cmd_message_prep($CMD{enable_bill_types}, $mess);
		}
		else {
			$rsp = $self->cmd_message_prep($CMD{enable_bill_types});
		}
return  $self->send_mess(\$rsp);
}

sub cmd_get_status {		# получение конфига (активные номиналы, включение усиленной проверки) валидатора 6 bytes
	my $self = shift;
	my $rsp = $self->cmd_message_prep($CMD{get_status});
	my $result = $self->send_mess(\$rsp); # $result = 0000f8000000
	print"^^^^^^^^^^^^^^" if DEBUG;
	p $result if DEBUG;
	return 0 if (($result eq '0')||($result eq '30')); # ошибки связи или команда применена неправильно 
	return '-1' if ($result eq '-1'); #ошибка проваливается из вызванной процедуры
	$VLD_state{get_status} = $result; # записываем в хэш состояния валидатора
	my @arr_d = unpack ('(a[2])*', $result);
		for ( my $i=0; $i<6; $i++ ){
			$arr_d[$i] = sprintf( "%b", hex($arr_d[$i]) );
			while ( length($arr_d[$i])<8 ){  
				$arr_d[$i] = '0'.$arr_d[$i]}; # добавляем нужное количество 0, чтобы получить битовый вектор байта
			}
			my $bill_type = $arr_d[0].$arr_d[1].$arr_d[2];
			my $security_level = $arr_d[3].$arr_d[4].$arr_d[5];
			my @arr_bt;
			my @arr_sl;
				unshift (@arr_bt, $_) for split ('', $bill_type);
				unshift (@arr_sl, $_) for split ('', $security_level);
				#p $bill_type; #debug
				my @bills_status;
				for (my $i=0; $i < @arr_bt;$i++){
					if ($arr_bt[$i] eq '0'){$arr_bt[$i] = 'bill off'}else{$arr_bt[$i] = 'bill on'}
					if ($arr_sl[$i] eq '0'){$arr_sl[$i] = 'sec off'}else{$arr_sl[$i] = 'sec on'}
					$bills_status[$i] = [$arr_bt[$i],$arr_sl[$i]];
				}
	$VLD_state{bills_status} = [@bills_status];
	p @bills_status if DEBUG; #debug
return 1; 
}

sub cmd_get_bill_table {		# получение списка принимаемых купюр 120 bytes !!!!
	my $self = shift;			# выдаёт неправильные значения отдельно от инициализации !!!
	my $rsp = $self->cmd_message_prep($CMD{get_bill_table}); # не получает список банкнот и перезаписывает переменную со списком номиналов !!!!!
	my $res = $self->send_mess(\$rsp);
	my @arr;
	my @arr2;
	return 0 if (($res eq '0')||($res eq '30')); # ошибки связи или команда применена неправильно 
	return '-1' if ($res eq '-1');
	##----
	##----
	my @arr5 = unpack '(a[10])*',$res; #разбиваем строку 120 на строки по 5 байт (24 купюры)

	say "!!!!!arr5 =" if DEBUG; #debug
	p @arr5 if DEBUG; # debug
		foreach (@arr5){
			if ($_ eq '0000000000'){
				push(@arr,'0');
				push(@arr2,'0');
				next; 
			}
			my $num = hex(substr($_,0,2));
			my $deg = hex(substr($_,-2));
			my $country = pack('H*', substr($_,2,6));
			my $bill = $num*10**$deg;
			push(@arr, $bill);
			push(@arr2, $bill.'-'.$country);
		}
	$VLD_state{bill_table} = \@arr; # массив значений номиналов без имени валюты
	$VLD_state{bill_table_cnt} = \@arr2; # массив номиналов купюр с именем валюты
	p %VLD_state if DEBUG; #debug
return 1; # успешно отработала элементы массива: номинал купюры с кодом страны (100-RUS, 1000-RUS)
}

sub cmd_identification { 
	my $self = shift;
	my $rsp = $self->cmd_message_prep($CMD{identification});
	my $res = $self->send_mess(\$rsp);
	# Сюда вставить обработку ошибок undef и неправильная команда
	return 0 if ($rsp eq '30');
	return '-1' if ($res eq '-1');
	my %hsh = (
		'Model' => pack('H*', substr($res,0,30)),           
		'Serial number' => pack('H*', substr($res,30,24)),   # номер не совпадает с лейбой (отдаёт 39К710015516 - лейба 39К711015516 
		'Asset number' =>  substr($res,-14),
	);
	$VLD_state{identification} = {%hsh};
return 1;
}

sub set_security{		# установка уровня защиты для каждой купюры 3 байта входных параметров в hex
	my ($self, $bill_sec_type) = @_; # 3 байта, тип безопасности купюры
	if (!defined($bill_sec_type)){ $bill_sec_type="000000"} # значение по умолчанию
	my $rsp = $self->cmd_message_prep($CMD{set_security}, $bill_sec_type);
	my $res = $self->send_mess(\$rsp);
	#if ($res eq '-1'){return '-1';} # CRC error
	#if ($res eq '00'){return '00';} # успешно подтверждена ACK
	#if ($res eq 'ff'){return 'ff'}; # команда не выполнена NACK
return $res; # код ошибки, неопределённое состояние
}

sub cmd_request_statistics{    # получение статистических данных по принятым и отклонённым купюрам
	my $self = shift;
	my $rsp = $self->cmd_message_prep($CMD{request_statistics});
	return $self->send_mess(\$rsp);
}

#--------- Рабочие последовательности для API ---------------------------
=head1
	Функции для API
	Представляют последовательности команд.
	На выходе массив данных
=cut
sub init_power_on {   # подготовка  к работе валидатора после подачи питания
	my $self = shift;
	my $res = $self->cmd_poll();
	   $res = $self->cmd_poll();
	   #p $res if DEBUG; #debug
	if ( ($response{$res} eq 'Power Up')||($response{$res} eq 'Power Up with Bill in Validator')||($response{$res} eq 'Power Up with Bill in Stacker')||($response{$res} eq 'Idling')||($response{$res} eq 'Initialize')||($response{$res} eq 'Unit Disabled')){
		$res = $self->cmd_reset;
		#p $res if DEBUG; #debug
		if ( $response{$res} eq 'ack' ){							
			$res = $self->cmd_poll();
			if ( $response{$res} eq 'Initialize' ){
				$res = $self->cmd_poll();
				#say "init_res = ".$res if DEBUG; #debug
				#--- wait сycle replacement sleep ---
				for (my $i=0; $i<20; $i++){
				$res = $self->cmd_poll();
				#say "init_res = ".$res if DEBUG; #debug
				last if ($response{$res} eq 'Unit Disabled');
				say"#### cycle $i #####" if DEBUG; #debug
				}
				#------------------------------------
					if ( $response{$res} eq 'Unit Disabled' ){
						$res = $self->cmd_get_status();
						if ( $res ) {
							$res  = $self->cmd_get_bill_table();
							if ($res){
								$res = $self->set_security();
								#p $res if DEBUG; #debug
								if ( $response{$res} eq 'ack' ){
									$res = $self->cmd_identification();
									#p $res if DEBUG; #debug
									if ($res){
										$res = $self->cmd_poll();
										#p $res if DEBUG; #debug
										if ( ($response{$res} eq 'Initialize')||($response{$res} eq 'Unit Disabled')) {
											do {
												say "+" if DEBUG; #debug
											} until ($response{$self->cmd_poll()} eq 'Unit Disabled');
											$VLD_state{init}=1; # валидатор инициализирован в модуле CCNET.
										        return {status=>1, response=>$res, description=>$response{$res}}; # инициализация выполнена	
										}
										return {status=>0, response=>$res, description=>$response{$res}};
									}
									return {status=>0, response=>$res, description=>$response{$res}};
								}
								return {status=>0, response=>$res, description=>$response{$res}};
							}
							return {status=>0, response=>$res, description=>$response{$res}};
						}
						return {status=>0, response=>$res, description=>$response{$res}};
					}
					return {status=>0, response=>$res, description=>$response{$res}};
			}
			return {status=>0, response=>$res, description=>$response{$res}};
		}
		return {status=>0, response=>$res, description=>$response{$res}};
	}
	return {status=>0, response=>$res, description=>$response{$res}}; # проблема инициализации
}

sub check_init {      # проверка инициализации валидатора в модуле, чтобы все внутренние переменные модуля были правильно заполнены
	my $self = shift;
	if ( $VLD_state{init} ){
		return {status=>1, description=>"validator initializing in module"};
	}
		return {status=>0, description=>"validator not initializing in module"};
}

sub escrow_on {      # разрешить приём купюр
	my $self = shift;
	$VLD_state{escrow}=[];	# подготавливаем переменные к счёту
	$VLD_state{escrow_sum}=0;
	#M2:	if (@{$VLD_state{bill_table}} eq 'undef'){
	#		my $status = $self->cmd_get_bill_table;
	#		if (!$status){goto M2;}
	#	}
	my $res = $self->cmd_enable_bill_types();
	if ($res){
		$res = $self->cmd_poll();	       	
		if ( $response{$res} eq 'Idling' ){ return {status=>1, response=>$res, description=>$response{$res}};} # купюры можно закидыввать
	}
	return {status=>0, response=>$res, description=>$response{$res}};
}

sub escrow_off {      # запретить приём купюр
	my $self = shift;
	my $B=$SET_BILLS;
	my $B_S=$SET_BILLS_SEC;
	$SET_BILLS='000000';
	$SET_BILLS_SEC='000000';
	my $res = $self->cmd_enable_bill_types();
	$SET_BILLS=$B;
	$SET_BILLS_SEC=$B_S;
	if ($res){
		$res = $self->cmd_poll();
		if ( $response{$res} eq 'Unit Disabled' ){ return {status=>1, response=>$res, description=>$response{$res}};} # закрываем приём купюр
	}
	return {status=>0, response=>$res, description=>$response{$res}};
}

sub bill_stack_pos {	# купюра вставлена в валидатор, но не сброшена. Возвращает  позицию в счёте десятичный вид (0-23)
	my $self = shift;
	my $res = $self->cmd_poll();
	while ($response{$res} eq 'Accepting'){
		$res = $self->cmd_poll();
	}
		p $res if DEBUG; #debug
		if ($res=~/80(..)/){	# ESCROW POSITION
			return {status=>1, response=>$res, description=>$response{$res}, result=>$VLD_state{bill_table}->[hex($1)]}; 
		}
	return {status=>0, response=>$res, description=>$response{$res}};
}

sub bill_stack_add {  # добавляем вставленную в валидатор купюру
	my $self = shift;
	my $res = $self->cmd_poll();
	if ( $res=~/80(..)/ ){
		$res = $self->cmd_stack();
		if ($res){
			$res = $self->cmd_poll(); 
			if ($response{$res} eq 'Stacking'){
				sleep (1);
				$res = $self->cmd_poll();
				if ($res =~/81(..)/){    # BILL STACKED
					my $nominal = $VLD_state{bill_table}->[hex($1)];
					#$res = $self->cmd_poll();
					#if ($response{$res} eq "Idling"){
						say "ADD bill_table parametr = ".$nominal if DEBUG;	#debug
						$VLD_state{escrow_sum} += $nominal; # меняем общую переменную состояния счёта
						p %VLD_state if DEBUG;
						push ( @{$VLD_state{escrow}}, $nominal );
						return {status=>1, response=>$res, description=>$response{$res},result=>$nominal};
						#}
				}
				return {status=>0, response=>$res, description=>$response{$res}};
			}
			return {status=>0, response=>$res, description=>$response{$res}};
		}
		return {status=>0, response=>$res, description=>$response{$res}};
	}
	return {status=>0, response=>$res, description=>$response{$res}};
}

sub bill_stack_return {  # возвращаем вставленную в валидатор купюру
	my $self = shift;
	my $res = $self->cmd_poll();
	if ( $res=~/80(..)/ ){
		$res = $self->cmd_return();
		if ($res){
			$res = $self->cmd_poll(); 
			if ($response{$res} eq 'Returning'){
				sleep (1);
				$res = $self->cmd_poll();
				if ($res=~/82(..)/){    # BILL RETURNED
					return {status=>1, response=>$res, description=>$response{$res}, result=>$VLD_state{bill_table}->[hex($1)]};
				}
				return {status=>0, response=>$res, description=>$response{$res}};
			}
			return {status=>0, response=>$res, description=>$response{$res}};
		}
		return {status=>0, response=>$res, description=>$response{$res}};
	}
	return {status=>0, response=>$res, description=>$response{$res}};
}

sub get_nominals_table{           # получаем таблицу с номиналами купюр
	my $self = shift;
	return {
		status=>1,
		result=>$VLD_state{bill_table},
	} if @{$VLD_state{bill_table}};
			p $VLD_state{bill_table} if DEBUG; #debug
return {
	status=>0,
	description=>'uninitialized vars'
}; 
}

sub get_approved_nominals{ # получаем номиналы купюр разрешённые к приему в упорядоченном массиве
	my $self =shift;
	my @status_arr;
	if ($self->cmd_get_status()){
		my $now_state = substr $VLD_state{get_status}, 0, 6; # забираем, нужные нам, 3 байта
		#foreach ($VLD_state{bills_status}){ # строим массив с реальной информакцией об включении по всем номиналам, уровень валидации опускаем
		#	push(@status_arr, $_[0]); 
		#}
		my $approved_nominals =((hex $SET_BILLS)&(hex $SET_BILLS_SEC));
		## --- обход прикола прошивки валидатора. Отдаёт в $now_state, что номинал отключен (200, 2000), хотя подали команду на его включение
		## --- поэтому нижнюю строчку пришлось отключить
		##>>>>>>>> $approved_nominals = $approved_nominals&(hex $now_state); # выбираем только те номиналы , которые разрешены
		## --- на другой прошивке можно попробовать включить верхнюю строку
		say "\$now_state = $now_state, \$approved_nominals = $approved_nominals" if DEBUG;
		my @approved_arr =split (//, sprintf( "%b",$approved_nominals ));
		@approved_arr = reverse @approved_arr;
		my $i=0;
		my @res_arr;
		foreach (@approved_arr){
			if ($_ eq "1"){
				push (@res_arr , $VLD_state{bill_table}[$i]);
			}
			$i++;
		}
	# убираем повоторы и сортируем массив купюр
		my %hash = map { $_ => 1} @res_arr;
		my @res = sort { $a <=> $b } keys %hash;
		return {
			status=>1,
			result=>[@res],
			description=>'array of approved bills'
		}
	}
	return {
	status=>0,
	description=>'uninitialized vars'
	}
}

sub get_escrow{           # получаем массив купюр полученных за сессию 
	my $self = shift;
	return {
		status=>1, 
		result=>$VLD_state{escrow},
	} if @{$VLD_state{escrow}};
return {
	status=>0,
	description=>'uninitialized vars'
}; 
}

sub get_escrow_sum{           # получаем сумму купюр полученных за сессию 
	my $self = shift;
	return {
		status=>1, 
		result=>$VLD_state{escrow_sum},
	} if defined $VLD_state{escrow_sum};
return {
	status=>0,
	description=>'uninitialized vars'
}; 
}

sub get_identification{      # получаем ссылку на хэш с прошивкой и серийными номерами 
	my $self = shift;
	return {
		status=>1, 
		result=>$VLD_state{identification}
	} if defined $VLD_state{identification};
return {status=>0,description=>'uninitialized vars'}; 
}

sub get_poll{      # API запрос состояния валидатора 
	my $self = shift;
	my $res = $self->cmd_poll();
	return { 
		status=>1, 
		response=>$res, 
		description=>$response{$res},
	};
}

sub get_request_statistics{      # API запрос на получение статистических данных по принятым и отклонённым купюрам
	my $self = shift;
	my $res = $self->cmd_request_statistics();
	return { 
		status=>1, 
		response=>$res, 
		description=>$response{$res},
	};
}
1;
#----- END OF CODE -----#


__END__


